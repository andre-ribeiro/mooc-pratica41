
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author andre
 */
public class Pratica41 {
    public static void main(String[] args) {
        Elipse e = new Elipse(1,2);
        Circulo c = new Circulo(3);
        
        System.out.println(e.getArea());
        System.out.println(e.getPerimetro());
        
        System.out.println(c.getArea());
        System.out.println(c.getPerimetro());
        
        
    }
}
