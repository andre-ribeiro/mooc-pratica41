/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author andre
 */
public class Circulo extends Elipse {
    
    public Circulo(double _r) {
        super(_r,_r);
    }
    
    @Override
    public double getPerimetro() {
        return 2*Math.PI*this.r;
    }
}
